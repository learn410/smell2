resource "aws_cloudwatch_log_group" "smell_log_group" {
  name = "/aws/lambda/${aws_lambda_function.smell_function.function_name}"

  retention_in_days = 1
}
