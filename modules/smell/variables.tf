variable "bucket_name" {
  description = "Name of the S3 Bucket Lambda zips are uploaded too"
  type        = string
  default     = "mezla-lambdas"
}

variable "ci_project_namespace" {
  description = "GitLab CI predefined variable"
  type        = string
}

variable "ci_project_name" {
  description = "GitLab CI predefined variable"
  type        = string
}
