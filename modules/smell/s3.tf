resource "aws_s3_bucket" "smell_bucket" {
  bucket = var.bucket_name

  tags = {
    Name = var.bucket_name
  }
}

resource "aws_s3_bucket_ownership_controls" "smell_bucket_ownership" {
  bucket = aws_s3_bucket.smell_bucket.id

  rule {
    object_ownership = "BucketOwnerEnforced"
  }
}

data "archive_file" "smell_lambda" {
  type             = "zip"
  source_dir       = "${path.root}/../../exe/"
  output_file_mode = "0666"
  output_path      = "${path.root}/smell.zip"
}

resource "aws_s3_object" "lambda_upload" {
  bucket = aws_s3_bucket.smell_bucket.id
  key    = "${var.ci_project_namespace}/${var.ci_project_name}/smell.zip"
  source = data.archive_file.smell_lambda.output_path
  etag   = filemd5(data.archive_file.smell_lambda.output_path)
}
