resource "aws_lambda_function" "smell_function" {
  function_name = "smell"
  s3_bucket     = aws_s3_bucket.smell_bucket.id
  s3_key        = aws_s3_object.lambda_upload.key
  runtime       = "go1.x"
  role          = aws_iam_role.lambda_exec.arn
  handler       = "main"
}

