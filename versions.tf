terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
    archive = {
      version = "~> 2.2"
    }
  }

  required_version = ">= 1.1.9"

  backend "http" {
  }
}

provider "aws" {
  region = "eu-west-1"
}
