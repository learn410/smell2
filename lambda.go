package main

import (
	"fmt"

	"github.com/aws/aws-lambda-go/lambda"
)

// HelloEvent triggers this lambda, contains a name.
type HelloEvent struct {
	Name string `json:"name"`
}

// HandleRequest is invoked by Lambda, and handles the event.
func HandleRequest(name HelloEvent) error {
	fmt.Printf("Hello %s!", name.Name) //nolint

	return nil
}

func main() {
	lambda.Start(HandleRequest)
}
